import UIKit


// MARK: - INITIALIZER -

class DIManagerOne {
    let title : UILabel?
    
    init(classTitle: UILabel) {
        self.title = classTitle
    }
}

let diManagerOne = DIManagerOne(classTitle: UILabel())


// MARK: - FUNCTION -

class DIManagerTwo {
    var title : UILabel?
    
    func setClassTitle(_ title: UILabel) {
        self.title = title
    }
}

let diManagerTwo = DIManagerTwo()
diManagerTwo.setClassTitle(UILabel())


// MARK: - PROTOCOL DELEGATE -

protocol DIManagerDelegate {
    func protocolMethod()
}

class DIManagerFour : DIManagerDelegate {
    
    func protocolMethod() {
         
    }
}


